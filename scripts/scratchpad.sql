﻿select vin
from dps.vehicleitems
group by vin having count(*) > 1


select groupnumber, uvc
from dps.blackbookresolver
group by groupnumber, uvc
having count(*) > 1

select *
from dps.blackbookresolver
where groupnumber = '4252'
  and uvc = '084'


select groupnumber, uvc
from dps.blackbookresolver
group by groupnumber, uvc

alter table dps.vehicleitems add primary key (vin)


select f.*, a.vin, a.yearmodel, a.make, a.model, a.bodystyle, a.trim,
  a.engine, a.transmission, a.vinresolved, 
  b.model_year,b.vin_division_name, b.vin_model_name, b.vin_style_name,
  b.engine_size, c.description, d.description,
   
from dps.vehicleitems a
left join chr.vin_pattern b on left(a.vin,8) = left(b.vin_pattern, 8)
  and a.vin like replace(vin_pattern, '*', '_')
left join chr.category c on b.engine_type_category_id = c.category_id 
left join chr.category d on b.fuel_type_category_id = d.category_id
inner join chr.vin_pattern_style_mapping e on b.vin_pattern_id = e.vin_pattern_id
inner join chr.year_make_model_style f on e.chrome_style_id = f.chrome_style_id
where a.make = 'chevrolet'  
  and b.vin_pattern is not null



-- 15 msec
DECLARE @vin string;
@vin = '2GTEK19K6S1501685';
SELECT *
FROM VinPattern
WHERE LEFT(VinPattern, 8) = LEFT(@vin, 8)
AND @vin LIKE REPLACE(vinpattern, '*', '_');

