import adsdb
import psycopg2
import csv
pg_con = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pg_cur = pg_con.cursor()
ads_con = adsdb.connect(DataSource='\\\\67.135.158.12:6363\\advantage\\dpsvseries\\dpsvseries.add',
                        userid='adssys', password='cartiva', CommType='TCP_IP', ServerType='remote',
                        TrimTrailingSpaces='TRUE')
ads_cur = ads_con.cursor()
tables = ["vehicleitems", "blackbookresolver", "makemodelclassifications"]
# file_name = 'dpsfiles/' + table + '.csv'
for table in tables:
    ads_cur.execute("select * from " + table)
    file_name = 'dpsfiles/' + table + '.csv'
    with open(file_name, 'wb') as f:
        csv.writer(f).writerows(ads_cur.fetchall())
        f.close()
        pg_cur.execute("truncate dps." + table)
        pg_con.commit()
        io = open(file_name, 'r')
        pg_cur.copy_expert("""copy dps.""" + table + """ from stdin with(format csv)""", io)
        pg_con.commit()
        io.close()
pg_cur.close()
ads_cur.close()
pg_con.close()
ads_con.close()

